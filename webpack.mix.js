const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

if (!mix.config.production) {
    mix.webpackConfig({devtool: "inline-source-map"}).sourceMaps();
}

mix.
    options({
        uglify: {
            uglifyOptions: {
                compress: {
                    drop_console: true,
                },
            },
        },
    }).
    setPublicPath("public").
    js("resources/assets/js/app.js", "public/js").version().
    extract([
        "vue",
    ]).
    autoload({
        jquery: ["$", "jQuery", "jquery", "window.jQuery"],
    }).
    sass("resources/assets/sass/app.scss", "public/css").version().
    sass("resources/assets/sass/print.scss", "public/css").version().
    copy("public", "./../../../public/vendor/invoices");

// mix.webpackConfig({
//     resolve: {
//         alias: {
//             "vue$": "vue/dist/vue.runtime.esm.js",
//         },
//     },
// });
