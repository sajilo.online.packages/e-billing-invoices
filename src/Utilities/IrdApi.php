<?php

namespace SajiloOnline\Invoices\Utilities;

use Carbon\Carbon;
use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Collection;
use SajiloOnline\Invoices\Model\Invoice;

/**
 * Class IrdApi
 * @package SajiloOnline\Invoices\Utilities
 */
class IrdApi
{
    const BASE_URL = 'http://202.166.207.75:9050/api';
    const USERNAME = 'Test_CBMS';
    const PASSWORD = 'test@321';

    /**
     * @var Guzzle
     */
    protected $client;
    /**
     * @var string
     */
    protected $url;
    /**
     * @var array
     */
    protected $data = [];
    /**
     * @var Invoice
     */
    protected $invoice;
    /**
     * @var Collection
     */
    protected $profile;

    /**
     * IrdApi constructor.
     *
     * @param Guzzle $client
     */
    public function __construct(Guzzle $client)
    {
        $this->client = $client;
    }

    /**
     * @param Invoice    $invoice
     * @param Collection $profile
     *
     * @return IrdApi
     */
    public function init(Invoice $invoice, Collection $profile)
    {
        $this->invoice = $invoice;
        $this->profile = $profile;

        return $this;
    }

    /**
     * Sync Invoice
     * @return bool
     */
    public function syncInvoice(): bool
    {
        try {
            logger()->info('Syncing invoice with IRD.');

            $this->prepare('bill')->request();
        } catch (GuzzleException $exception) {
            logger()->error($exception);

            return false;
        }

        return true;
    }

    /**
     * Sync Invoice
     * @return bool
     */
    public function syncSalesReturn(): bool
    {
        try {
            logger()->info('Syncing sales return with IRD.');

            $this->prepare('billreturn')->request();
        } catch (GuzzleException $exception) {
            logger()->error($exception);

            return false;
        }

        return true;
    }

    /**
     * @throws GuzzleException
     */
    protected function request()
    {
        $this->client->request(
            'POST',
            $this->url,
            [
                'json' => $this->data,
            ]
        );
    }

    /**
     * @param string $type
     *
     * @return IrdApi
     */
    protected function prepare($type = 'bill')
    {
        $endpoint   = $type === 'bill' ? 'bill' : 'billreturn';
        $this->url  = $this->getUrl($endpoint);
        $this->data = $type === 'bill' ? $this->prepareInvoiceData() : $this->prepareSalesReturnData();

        return $this;
    }

    /**
     * @param string $endpoint
     *
     * @return string
     */
    protected function getUrl(string $endpoint): string
    {
        return sprintf("%s/%s", lrtrim(self::BASE_URL, '/'), lrtrim($endpoint));
    }

    /**
     * @return array
     */
    protected function prepareInvoiceData(): array
    {
        return [
            "username"           => self::USERNAME,
            "password"           => self::PASSWORD,
            "seller_pan"         => $this->profile->get('pan'),
            "buyer_pan"          => $this->invoice->customer_pan,
            "fiscal_year"        => $this->invoice->fiscal_year->title,
            "buyer_name"         => $this->invoice->customer_name,
            "invoice_number"     => $this->invoice->bill_no,
            "invoice_date"       => $this->formatDate($this->invoice->issue_date_bs),
            "total_sales"        => $this->invoice->total_amount,
            "taxable_sales_vat"  => $this->invoice->taxable_amount,
            "vat"                => $this->invoice->tax_amount,
            "excisable_amount"   => 0,
            "excise"             => 0,
            "taxable_sales_hst"  => 0,
            "hst"                => 0,
            "amount_for_esf"     => 0,
            "esf"                => 0,
            "export_sales"       => 0,
            "tax_exempted_sales" => 0,
            "isrealtime"         => $this->invoice->is_real_time,
            "datetimeClient"     => Carbon::now()->toDateTimeString(),
        ];
    }

    /**
     * @return array
     */
    protected function prepareSalesReturnData(): array
    {
        return [
            "username"           => self::USERNAME,
            "password"           => self::PASSWORD,
            "seller_pan"         => $this->profile->get('pan'),
            "buyer_pan"          => $this->invoice->customer_pan,
            "fiscal_year"        => $this->invoice->fiscal_year->title,
            "buyer_name"         => $this->invoice->customer_name,
            "ref_invoice_number" => $this->invoice->bill_no,
            "credit_note_number" => $this->invoice->cancelled_invoice->credit_note_number,
            "credit_note_date"   => $this->formatDate($this->invoice->cancelled_invoice->credit_note_date_bs),
            "reason_for_return"  => $this->invoice->cancelled_invoice->reason_for_return,
            "total_sales"        => $this->invoice->total_amount,
            "taxable_sales_vat"  => $this->invoice->taxable_amount,
            "vat"                => $this->invoice->tax_amount,
            "excisable_amount"   => 0,
            "excise"             => 0,
            "taxable_sales_hst"  => 0,
            "hst"                => 0,
            "amount_for_esf"     => 0,
            "esf"                => 0,
            "export_sales"       => 0,
            "tax_exempted_sales" => 0,
            "isrealtime"         => $this->invoice->cancelled_invoice->is_real_time,
            "datetimeClient"     => Carbon::now()->toDateTimeString(),
        ];
    }

    /**
     * @param string $date
     *
     * @return string
     */
    protected function formatDate(string $date): string
    {
        list($year, $month, $day) = explode('-', $date);

        return sprintf("%s.%s.%s", $year, zero_pad($month, 2), zero_pad($day, 2));
    }
}
