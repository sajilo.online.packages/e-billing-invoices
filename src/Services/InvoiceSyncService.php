<?php

namespace SajiloOnline\Invoices\Services;

use Illuminate\Support\Collection;
use SajiloOnline\Invoices\Model\Invoice;
use SajiloOnline\Invoices\Utilities\IrdApi;

/**
 * Class InvoiceSyncService
 * @package SajiloOnline\Invoices\Services
 */
class InvoiceSyncService
{
    /**
     * @var IrdApi
     */
    protected $irdApi;
    /**
     * @var InvoiceService
     */
    protected $invoiceService;

    /**
     * InvoiceSyncService constructor.
     *
     * @param IrdApi         $irdApi
     * @param InvoiceService $invoiceService
     */
    public function __construct(IrdApi $irdApi, InvoiceService $invoiceService)
    {
        $this->irdApi         = $irdApi;
        $this->invoiceService = $invoiceService;
    }

    /**
     * @param Invoice    $invoice
     * @param Collection $profile
     */
    public function syncInvoiceRealTime(Invoice $invoice, Collection $profile)
    {
        if ( !$invoice->is_real_time || $invoice->sync_with_ird ) {
            return;
        }

        $status = $this->proceedInvoiceSync($invoice, $profile);

        $update = [
            'is_real_time'  => $status,
            'sync_with_ird' => $status,
        ];

        $this->invoiceService->update($update, $invoice);
    }

    /**
     * @param Invoice    $invoice
     * @param Collection $profile
     */
    public function syncSalesReturnRealTime(Invoice $invoice, Collection $profile)
    {
        if ( !$invoice->cancelled_invoice || !$invoice->cancelled_invoice->is_real_time || $invoice->cancelled_invoice->sync_with_ird ) {
            return;
        }

        $status = $this->proceedSalesReturnSync($invoice, $profile);

        $update = [
            'is_real_time'  => $status,
            'sync_with_ird' => $status,
        ];

        $invoice->cancelled_invoice()->update($update);
    }

    /**
     * @param Invoice    $invoice
     * @param Collection $profile
     *
     * @return bool
     */
    protected function proceedInvoiceSync(Invoice $invoice, Collection $profile): bool
    {
        return $this->irdApi->init($invoice, $profile)->syncInvoice();
    }

    /**
     * @param Invoice    $invoice
     * @param Collection $profile
     *
     * @return bool
     */
    protected function proceedSalesReturnSync(Invoice $invoice, Collection $profile): bool
    {
        return $this->irdApi->init($invoice, $profile)->syncSalesReturn();
    }
}
