<?php

namespace SajiloOnline\Invoices\Services;

use App\Data\Repositories\Company\ProfileRepository;
use App\Infrastructure\Events\InvoiceAdded;
use App\Infrastructure\Events\SalesReturned;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use SajiloOnline\Invoices\Contracts\CancelledInvoiceRepository;
use SajiloOnline\Invoices\Contracts\InvoiceRepository;
use SajiloOnline\Invoices\Model\CancelledInvoice;
use SajiloOnline\Invoices\Model\Invoice;

/**
 * Class InvoiceService
 * @package SajiloOnline\Invoices\Services
 */
class InvoiceService
{
    /**
     * @var InvoiceRepository
     */
    protected $invoiceRepository;
    /**
     * @var CancelledInvoiceRepository
     */
    protected $cancelledInvoiceRepository;
    /**
     * @var ProfileRepository
     */
    protected $profileRepository;

    /**
     * InvoiceService constructor.
     *
     * @param InvoiceRepository          $invoiceRepository
     * @param CancelledInvoiceRepository $cancelledInvoiceRepository
     * @param ProfileRepository          $profileRepository
     */
    public function __construct(
        InvoiceRepository $invoiceRepository,
        CancelledInvoiceRepository $cancelledInvoiceRepository,
        ProfileRepository $profileRepository
    ) {
        $this->invoiceRepository          = $invoiceRepository;
        $this->cancelledInvoiceRepository = $cancelledInvoiceRepository;
        $this->profileRepository          = $profileRepository;
    }

    /**
     * @return int
     */
    public function generateBillNumber(): int
    {
        $maxBillNum = $this->invoiceRepository->getMaxBillNum();

        return $maxBillNum + 1;
    }

    /**
     * @param array $invoiceData
     *
     * @return Invoice
     */
    public function create(array $invoiceData): Invoice
    {
        $invoice = $this->invoiceRepository->create($invoiceData);
        $profile = $this->profileRepository->profileData();

        event(new InvoiceAdded($invoice, $profile));

        return $invoice;
    }

    /**
     * @param int $invoiceId
     *
     * @return Invoice
     */
    public function getById(int $invoiceId): Invoice
    {
        return $this->invoiceRepository->find($invoiceId);
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->invoiceRepository->all();
    }

    /**
     * @return Collection
     */
    public function getAllActive(): Collection
    {
        return $this->invoiceRepository->findWhere(['is_bill_active' => true]);
    }

    /**
     * @return Collection
     */
    public function getAllCancelled(): Collection
    {
        return $this->invoiceRepository->findWhere(['is_bill_active' => false]);
    }

    /**
     * @param string $reportType
     * @param string $dateType
     * @param int    $year
     * @param int    $month
     *
     * @return Collection
     */
    public function filterByDate(string $reportType, string $dateType, int $year, int $month): Collection
    {
        $dateFrom = $dateType === 'ad' ? sprintf("%d-%02d-01", $year, $month) : sprintf("%d-%d-01", $year, $month);
        $dateTo   = $dateType === 'ad' ? sprintf("%d-%02d-31", $year, $month) : sprintf("%d-%d-32", $year, $month);

        return $this->invoiceRepository->getAllByDateRange($reportType, $dateType, $dateFrom, $dateTo);
    }

    /**
     * @param array   $cancelledData
     * @param Invoice $invoice
     *
     * @return CancelledInvoice
     */
    public function cancelInvoice(array $cancelledData, Invoice $invoice): CancelledInvoice
    {
        $this->invoiceRepository->update(['is_bill_active' => false], $invoice->id);

        $cancelledInvoice = $this->cancelledInvoiceRepository->create($cancelledData);

        $updatedInvoice = $this->invoiceRepository->find($invoice->id);
        $profile        = $this->profileRepository->profileData();

        event(new SalesReturned($updatedInvoice, $profile));

        return $cancelledInvoice;
    }

    /**
     * @param string $date
     *
     * @return int
     */
    public function getCreditNoteNumber(string $date): int
    {
        return $this->cancelledInvoiceRepository->generateCreditNoteNumber($date);
    }

    /**
     * @param int $invoiceId
     *
     * @return Invoice
     */
    public function updatePrintStatus(int $invoiceId): Invoice
    {
        $printData = [
            'is_bill_printed' => true,
            'printed_time'    => Carbon::now(),
            'printed_by_id'   => currentUser()->id,
        ];

        return $this->invoiceRepository->update($printData, $invoiceId);
    }

    /**
     * @param array   $attributes
     * @param Invoice $invoice
     *
     * @return Invoice
     */
    public function update(array $attributes, Invoice $invoice): Invoice
    {
        return $this->invoiceRepository->update($attributes, $invoice->id);
    }
}
