<?php

use Illuminate\Support\Collection;
use SajiloOnline\Invoices\Model\Invoice;

if ( !function_exists('amountFormat') ) {
    /**
     * @param float $amount
     *
     * @return string
     */
    function amountFormat(float $amount): string
    {
        return number_format($amount, 2, '.', ',');
    }
}

if ( !function_exists('calculate_total') ) {
    /**
     * @param Collection $invoices
     * @param string     $key
     *
     * @return string
     */
    function calculate_total(Collection $invoices, string $key): string
    {
        $total = $invoices->map(
            function (Invoice $invoice) use ($key) {
                return $invoice->$key;
            }
        )->sum();

        return amountFormat($total);
    }
}

if ( !function_exists('month_name') ) {
    /**
     * @param string $dateType
     * @param int    $month
     *
     * @return string
     */
    function month_name(string $dateType, int $month): string
    {
        $ad = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        $bs = ["बैशाख", "जेठ", "असार", "सावन", "भदौ", "असोज", "कार्तिक", "मंसिर", "पौष", "माघ", "फागुन", "चैत"];

        return $$dateType[$month - 1];
    }
}

if ( !function_exists('lrtrim') ) {
    /**
     * @param string $string
     * @param string $charlist
     *
     * @return string
     */
    function lrtrim(string $string, $charlist = " \t\n\r\0\x0B"): string
    {
        return ltrim(rtrim($string, $charlist), $charlist);
    }
}

if ( !function_exists('zero_pad') ) {
    /**
     * @param string $string
     * @param int    $padLength
     *
     * @return string
     */
    function zero_pad(string $string, int $padLength): string
    {
        return str_pad($string, $padLength, '0', STR_PAD_LEFT);
    }
}
