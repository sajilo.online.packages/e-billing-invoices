<?php

namespace SajiloOnline\Invoices\Constants;

/**
 * Class InvoiceType
 * @package SajiloOnline\Invoices\Constants
 */
/**
 * Class InvoiceType
 * @package SajiloOnline\Invoices\Constants
 */
class InvoiceType
{
    const TAX_INVOICE = 'tax-invoice';
    const TAX_ABBR    = 'tax-abbr';
    const INCOME_TAX  = 'income-tax';

    /**
     * @return array
     */
    public static function ALL(): array
    {
        return [
            self::TAX_INVOICE,
            self::TAX_ABBR,
            self::INCOME_TAX,
        ];
    }
}
