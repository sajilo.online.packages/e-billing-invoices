<?php

namespace SajiloOnline\Invoices\Http\Controllers;

use App\Domain\Web\Services\Company\ProfileService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use SajiloOnline\Invoices\Constants\InvoiceType;
use SajiloOnline\Invoices\Http\Requests\InvoiceCancelRequest;
use SajiloOnline\Invoices\Http\Requests\InvoiceStoreRequest;
use SajiloOnline\Invoices\Services\InvoiceService;

/**
 * Class InvoicesController
 * @package SajiloOnline\Invoices\Http\Controllers
 */
class InvoicesController extends Controller
{
    /**
     * @var ProfileService
     */
    protected $profileService;
    /**
     * @var InvoiceService
     */
    protected $invoiceService;

    /**
     * InvoicesController constructor.
     *
     * @param ProfileService $profileService
     * @param InvoiceService $invoiceService
     */
    public function __construct(
        ProfileService $profileService,
        InvoiceService $invoiceService
    ) {
        parent::__construct();

        $this->profileService = $profileService;
        $this->invoiceService = $invoiceService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $activeInvoices    = $this->invoiceService->getAllActive();
        $cancelledInvoices = $this->invoiceService->getAllCancelled();

        return view('invoices::invoice-list', compact('activeInvoices', 'cancelledInvoices'));
    }

    /**
     * @param $invoiceType
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($invoiceType)
    {
        if ( !in_array($invoiceType, InvoiceType::ALL()) ) {
            return abort(404);
        }

        $seller = $this->profileService->getProfile();

        return view('invoices::invoice-form', compact('invoiceType', 'seller'));
    }

    /**
     * @param InvoiceStoreRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(InvoiceStoreRequest $request)
    {
        try {
            $invoice = $request->set()->persist();
        } catch (\Exception $exception) {
            logger()->error($exception);

            return $this->errorJsonResponse($exception->getMessage());
        }

        return $this->jsonResponse(
            [
                'redirect_url' => route('invoices.show', $invoice->id),
            ],
            'Saved Successfully.'
        );
    }

    /**
     * @param $invoiceId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($invoiceId)
    {
        try {
            $invoice = $this->invoiceService->getById((int) $invoiceId);
        } catch (\Exception $exception) {
            return abort(404);
        }

        $seller = $this->profileService->getProfile();

        return view('invoices::invoice-detail', compact('invoice', 'seller'));
    }

    /**
     * @param                      $invoiceId
     *
     * @param InvoiceCancelRequest $request =
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel($invoiceId, InvoiceCancelRequest $request)
    {
        try {
            $request->setInvoice((int) $invoiceId)->set()->persist();
        } catch (ModelNotFoundException $exception) {
            return abort(404);
        }

        return $this->jsonResponse(
            [
                'redirect_url' => route('invoices.show', $invoiceId),
            ]
        );
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function creditNoteNumber()
    {
        $date = request()->get('date');

        $number = $this->invoiceService->getCreditNoteNumber($date);

        return $this->jsonResponse($number);
    }

    /**
     * @param $invoiceId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function print($invoiceId)
    {
        $invoice = $this->invoiceService->updatePrintStatus((int) $invoiceId);

        return $this->jsonResponse(
            [
                'redirect_url' => route('invoices.show', $invoiceId),
            ]
        );
    }
}
