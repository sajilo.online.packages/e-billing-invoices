<?php

namespace SajiloOnline\Invoices\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use SajiloOnline\Invoices\Http\Middleware\Authenticate;
use SajiloOnline\Invoices\Traits\JsonResponse;

/**
 * Class Controller
 * @package SajiloOnline\Invoices\Http\Controllers
 */
class Controller extends BaseController
{
    use JsonResponse;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->middleware(Authenticate::class);
    }

    /**
     * @param string $error
     * @param int    $code
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function errorJsonResponse(string $error = 'error', int $code = 500)
    {
        return response()->json($this->makeError($error), $code);
    }

    /**
     * @param mixed  $result
     * @param string $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function jsonResponse($result, string $message = 'success')
    {
        return response()->json($this->makeResponse($message, $result), 200);
    }
}
