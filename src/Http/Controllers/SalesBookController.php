<?php

namespace SajiloOnline\Invoices\Http\Controllers;

use App\Domain\Web\Services\Company\ProfileService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use SajiloOnline\Invoices\Http\Exports\SalesBook\ExportToExcel;
use SajiloOnline\Invoices\Services\InvoiceService;

/**
 * Class SalesBookController
 * @package SajiloOnline\Invoices\Http\Controllers
 */
class SalesBookController extends Controller
{
    /**
     * @var InvoiceService
     */
    protected $invoiceService;
    /**
     * @var ProfileService
     */
    protected $profileService;

    /**
     * SalesBookController constructor.
     *
     * @param InvoiceService $invoiceService
     * @param ProfileService $profileService
     */
    public function __construct(
        InvoiceService $invoiceService,
        ProfileService $profileService
    ) {
        parent::__construct();

        $this->invoiceService = $invoiceService;
        $this->profileService = $profileService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('invoices::sales-book', ['type' => 'sales-book']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function salesReturn()
    {
        return view('invoices::sales-book', ['type' => 'sales-return']);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function generate(Request $request)
    {
        $reportType = $request->get('report_type');
        $dateType   = $request->get('date_type');
        $year       = $request->get('year');
        $month      = $request->get('month');

        $invoices = $this->invoiceService->filterByDate($reportType, $dateType, (int) $year, (int) $month);
        $seller   = $this->profileService->getProfile();

        return view(
            'invoices::partials.sales-book-table',
            compact(
                'invoices',
                'seller',
                'year',
                'month',
                'dateType',
                'reportType'
            )
        );
    }

    /**
     * @param         $type
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \Throwable
     */
    public function export($type, Request $request)
    {
        $type       = strtolower($type);
        $reportType = $request->get('report_type');
        $dateType   = $request->get('date_type');
        $year       = $request->get('year');
        $month      = $request->get('month');

        $invoices = $this->invoiceService->filterByDate($reportType, $dateType, (int) $year, (int) $month);
        $seller   = $this->profileService->getProfile();

        if ( !in_array($type, ['pdf', 'excel']) ) {
            return abort(404);
        }

        $fileName = sprintf("%s-%s-%s-%s", $reportType === 'active' ? 'sales-book' : 'sales-return', $month, $year, $dateType);

        $viewData = compact(
            'invoices',
            'seller',
            'year',
            'month',
            'dateType',
            'reportType'
        );

        if ( $type === 'pdf' ) {
            $pdf = \PDF::loadView(
                'invoices::sales-book-pdf',
                $viewData
            );

            return $pdf->download($fileName.".pdf");
        }

//        return (new ExportToExcel($viewData))->view();
        return Excel::download(new ExportToExcel($viewData), $fileName.'.xlsx');
    }
}
