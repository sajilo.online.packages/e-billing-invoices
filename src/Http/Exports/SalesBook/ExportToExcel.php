<?php

namespace SajiloOnline\Invoices\Http\Exports\SalesBook;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

/**
 * Class ExportToExcel
 * @package SajiloOnline\Invoices\Http\Exports\SalesBook
 */
class ExportToExcel implements FromView
{
    /**
     * @var array
     */
    protected $data;

    /**
     * ExportToExcel constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return View
     */
    public function view(): View
    {
        return view('invoices::sales-book-excel', $this->data);
    }
}
