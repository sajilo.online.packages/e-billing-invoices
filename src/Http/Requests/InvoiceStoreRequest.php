<?php

namespace SajiloOnline\Invoices\Http\Requests;

use App\Domain\Web\Services\FiscalYear\FiscalYearsService;
use Illuminate\Database\DatabaseManager;
use Illuminate\Foundation\Http\FormRequest;
use SajiloOnline\Invoices\Model\Invoice;
use SajiloOnline\Invoices\Services\InvoiceService;

/**
 * Class InvoiceStoreRequest
 * @package SajiloOnline\Invoices\Http\Requests
 */
class InvoiceStoreRequest extends FormRequest
{
    /**
     * @var array
     */
    protected $invoiceData = [];
    /**
     * @var array
     */
    protected $invoiceDetails = [];
    /**
     * @var FiscalYearsService
     */
    protected $fiscalYearsService;
    /**
     * @var InvoiceService
     */
    protected $invoiceService;
    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * InvoiceStoreRequest constructor.
     *
     * @param FiscalYearsService $fiscalYearsService
     * @param InvoiceService     $invoiceService
     * @param DatabaseManager    $databaseManager
     */
    public function __construct(
        FiscalYearsService $fiscalYearsService,
        InvoiceService $invoiceService,
        DatabaseManager $databaseManager
    ) {
        parent::__construct();

        $this->fiscalYearsService = $fiscalYearsService;
        $this->invoiceService     = $invoiceService;
        $this->databaseManager    = $databaseManager;
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */

    public function rules(): array
    {
        return [
            'type'  => 'required',
            'total' => 'required',
        ];
    }

    /**
     * @return $this
     */
    public function set()
    {
        $fiscalYear = $this->fiscalYearsService->getCurrent();

        $purchaser  = $this->request->get('purchaser');
        $transDate  = $this->request->get('trans_date');
        $issuedDate = $this->request->get('issued_date');
        $discount   = $this->request->get('discount');

        $this->invoiceData = [
            'invoice_type'        => $this->request->get('type'),
            'fiscal_year_id'      => $fiscalYear->id,
            'bill_no'             => $this->invoiceService->generateBillNumber(),
            'customer_name'       => $purchaser['name'],
            'customer_pan'        => $purchaser['pan'],
            'customer_address'    => $purchaser['address'],
            'bill_date_ad'        => $transDate['ad'],
            'bill_date_bs'        => $transDate['bs'],
            'issue_date_ad'       => $issuedDate['ad'],
            'issue_date_bs'       => $issuedDate['bs'],
            'payment_method'      => $this->request->get('payment_method'),
            'amount'              => $this->request->get('amount'),
            'discount_percentage' => $discount['percentage'],
            'discount_amount'     => $discount['amount'],
            'taxable_amount'      => $this->request->get('taxable_amount'),
            'tax_amount'          => $this->request->get('vat'),
            'total_amount'        => $this->request->get('total'),
            'total_in_words'      => $this->request->get('total_in_words'),
        ];

        $this->invoiceDetails = $this->request->get('details');

        return $this;
    }

    /**
     * @return Invoice
     * @throws \Exception
     */
    public function persist(): Invoice
    {
        $this->databaseManager->beginTransaction();

        try {
            $invoice = $this->invoiceService->create($this->invoiceData);

            collect($this->invoiceDetails)->each(
                function (array $detail) use ($invoice) {
                    $data = [
                        'sn'         => $detail['sn'],
                        'particular' => $detail['particular'],
                        'quantity'   => $detail['quantity'],
                        'rate'       => $detail['rate'],
                        'total'      => $detail['total'],
                    ];

                    $invoice->details()->create($data);
                }
            );

            $this->databaseManager->commit();
        } catch (\Exception $exception) {
            $this->databaseManager->rollBack();
            throw $exception;
        }

        return $invoice;
    }
}
