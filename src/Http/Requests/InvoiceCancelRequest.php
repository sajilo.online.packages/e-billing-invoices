<?php

namespace SajiloOnline\Invoices\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use SajiloOnline\Invoices\Model\Invoice;
use SajiloOnline\Invoices\Services\InvoiceService;

/**
 * Class InvoiceCancelRequest
 * @package SajiloOnline\Invoices\Http\Requests
 */
class InvoiceCancelRequest extends FormRequest
{
    /**
     * @var InvoiceService
     */
    protected $invoiceService;
    /**
     * @var Invoice
     */
    protected $invoice;
    /**
     * @var array
     */
    protected $cancelledData = [];

    /**
     * InvoiceCancelRequest constructor.
     *
     * @param InvoiceService $invoiceService
     */
    public function __construct(InvoiceService $invoiceService)
    {
        parent::__construct();

        $this->invoiceService = $invoiceService;
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'credit_note_number'  => 'required',
            'credit_note_date_ad' => 'required',
            'credit_note_date_bs' => 'required',
            'reason_for_return'   => 'nullable',
        ];
    }

    /**
     * @param int $invoiceId
     *
     * @return $this
     */
    public function setInvoice(int $invoiceId)
    {
        $this->invoice = $this->invoiceService->getById($invoiceId);

        return $this;
    }

    /**
     * @return $this
     */
    public function set()
    {
        $this->cancelledData = [
            'invoice_id'          => $this->invoice->id,
            'credit_note_number'  => $this->request->get('credit_note_number'),
            'credit_note_date_ad' => $this->request->get('credit_note_date_ad'),
            'credit_note_date_bs' => $this->request->get('credit_note_date_bs'),
            'reason_for_return'   => $this->request->get('reason_for_return'),
        ];

        return $this;
    }

    /**
     * Proceed Invoice cancellation
     */
    public function persist()
    {
        $this->invoiceService->cancelInvoice($this->cancelledData, $this->invoice);
    }
}
