<?php

namespace SajiloOnline\Invoices\Http\Middleware;

/**
 * Class Authenticate
 * @package SajiloOnline\Invoices\Http\Middleware
 */
class Authenticate
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return \Illuminate\Http\Response|null
     */
    public function handle($request, $next)
    {
        return $next($request);
//        return Invoices::check($request) ? $next($request) : abort(403);
    }
}
