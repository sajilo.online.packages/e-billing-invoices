<?php

namespace SajiloOnline\Invoices\Contracts;

use Illuminate\Support\Collection;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface InvoiceRepository
 * @package SajiloOnline\Invoices\Contracts
 */
interface InvoiceRepository extends RepositoryInterface
{
    /**
     * @return int
     */
    public function getMaxBillNum(): int;

    /**
     * @param string $reportType
     * @param string $dateType
     * @param string $dateFrom
     * @param string $dateTo
     *
     * @return Collection|array
     */
    public function getAllByDateRange(string $reportType, string $dateType, string $dateFrom, string $dateTo);
}
