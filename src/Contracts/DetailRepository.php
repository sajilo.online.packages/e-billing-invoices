<?php

namespace SajiloOnline\Invoices\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DetailRepository
 * @package SajiloOnline\Invoices\Contracts
 */
interface DetailRepository extends RepositoryInterface
{

}
