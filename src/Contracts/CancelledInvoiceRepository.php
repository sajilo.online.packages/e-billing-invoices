<?php

namespace SajiloOnline\Invoices\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CancelledInvoiceRepository
 * @package SajiloOnline\Invoices\Contracts
 */
interface CancelledInvoiceRepository extends RepositoryInterface
{

    /**
     * @param string $date
     *
     * @return int
     */
    public function generateCreditNoteNumber(string $date): int;
}
