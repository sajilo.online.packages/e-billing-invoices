<?php

namespace SajiloOnline\Invoices\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use SajiloOnline\Invoices\Contracts\DetailRepository;
use SajiloOnline\Invoices\Model\Detail;

/**
 * Class DetailEloquentRepository
 * @package SajiloOnline\Invoices\Repositories
 */
class DetailEloquentRepository extends BaseRepository implements DetailRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Detail::class;
    }
}
