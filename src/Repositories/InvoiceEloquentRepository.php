<?php

namespace SajiloOnline\Invoices\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Prettus\Repository\Eloquent\BaseRepository;
use SajiloOnline\Invoices\Contracts\InvoiceRepository;
use SajiloOnline\Invoices\Model\Invoice;

/**
 * Class InvoiceEloquentRepository
 * @package SajiloOnline\Invoices\Repositories
 */
class InvoiceEloquentRepository extends BaseRepository implements InvoiceRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Invoice::class;
    }

    /**
     * @param string $reportType
     * @param string $dateType
     * @param string $dateFrom
     * @param string $dateTo
     *
     * @return Collection|array
     */
    public function getAllByDateRange(string $reportType, string $dateType, string $dateFrom, string $dateTo)
    {
        /** @var Builder $model */
        $model = $this->model;

        $model = $model->where('is_bill_active', $reportType === 'active');

        if ( $dateType === 'ad' ) {
            $model = $model->whereBetween('bill_date_ad', [$dateFrom, $dateTo]);
        }

        if ( $dateType === 'bs' ) {
            $model = $model->where('bill_date_bs', '>=', $dateFrom)->Where('bill_date_bs', '<=', $dateTo);
        }

        $model = $model->orderBy('bill_date_ad', 'asc')->orderBy('bill_no', 'asc');

        return $this->parserResult($model->get());
    }

    /**
     * @return int
     */
    public function getMaxBillNum(): int
    {
        /** @var Builder $model */
        $model = $this->model;

        $bill       = $model->selectRaw(\DB::raw('MAX(bill_no) as max_bill_num'))->get();
        $maxBillNum = $bill->first()->max_bill_num;

        return is_null($maxBillNum) ? 0 : $maxBillNum;
    }
}
