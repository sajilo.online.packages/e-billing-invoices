<?php

namespace SajiloOnline\Invoices\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Eloquent\BaseRepository;
use SajiloOnline\Invoices\Contracts\CancelledInvoiceRepository;
use SajiloOnline\Invoices\Model\CancelledInvoice;

/**
 * Class CancelledInvoiceEloquentRepository
 * @package SajiloOnline\Invoices\Repositories
 */
class CancelledInvoiceEloquentRepository extends BaseRepository implements CancelledInvoiceRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CancelledInvoice::class;
    }

    /**
     * @param string $date
     *
     * @return int
     */
    public function generateCreditNoteNumber(string $date): int
    {
        /** @var Builder $model */
        $model = $this->model;

        $bill                = $model->selectRaw(\DB::raw('MAX(credit_note_number) as max_credit_note_number'))->where('credit_note_date_ad', $date)->get();
        $maxCreditNoteNumber = $bill->first()->max_credit_note_number;

        return is_null($maxCreditNoteNumber) ? 1 : $maxCreditNoteNumber + 1;
    }
}
