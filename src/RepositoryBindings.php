<?php

namespace SajiloOnline\Invoices;

use SajiloOnline\Invoices\Contracts\CancelledInvoiceRepository;
use SajiloOnline\Invoices\Contracts\DetailRepository;
use SajiloOnline\Invoices\Contracts\InvoiceRepository;
use SajiloOnline\Invoices\Repositories\CancelledInvoiceEloquentRepository;
use SajiloOnline\Invoices\Repositories\DetailEloquentRepository;
use SajiloOnline\Invoices\Repositories\InvoiceEloquentRepository;

/**
 * Trait RepositoryBindings
 * @package SajiloOnline\Invoices
 */
trait RepositoryBindings
{
    /**
     * All of the repository bindings
     *
     * @var array
     */
    public $repositoryBindings = [
        InvoiceRepository::class          => InvoiceEloquentRepository::class,
        DetailRepository::class           => DetailEloquentRepository::class,
        CancelledInvoiceRepository::class => CancelledInvoiceEloquentRepository::class,
    ];
}
