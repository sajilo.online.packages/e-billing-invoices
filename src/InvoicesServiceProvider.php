<?php

namespace SajiloOnline\Invoices;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

/**
 * Class InvoicesServiceProvider
 * @package SajiloOnline\Invoices
 */
class InvoicesServiceProvider extends ServiceProvider
{
    use RepositoryBindings;

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerHelpers();
        $this->registerRoutes();
        $this->registerResources();
        $this->defineAssetPublishing();
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->configure();
        $this->offerPublishing();
        $this->registerRepositories();
    }

    /**
     * Register helpers file
     */
    protected function registerHelpers()
    {
        return require_once(__DIR__.'/Helpers/common.php');
    }

    /**
     * Register the routes.
     *
     * @return void
     */
    protected function registerRoutes()
    {
        Route::group(
            [
                'prefix'     => 'new/invoices',
                'namespace'  => 'SajiloOnline\Invoices\Http\Controllers',
                'middleware' => ['web','auth'],
                'as'         => 'invoices.',
            ],
            function () {
                $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
            }
        );
    }

    /**
     * Register the resources.
     *
     * @return void
     */
    protected function registerResources()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'invoices');
    }

    /**
     * Setup the configuration
     *
     * @return void
     */
    protected function configure()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/invoices.php', 'invoices');
    }

    /**
     * Setup the resource publishing groups
     *
     * @return void
     */
    protected function offerPublishing()
    {
        if ( $this->app->runningInConsole() ) {
            $this->publishes(
                [
                    __DIR__.'/../config/invoices.php' => config_path('invoices.php'),
                ],
                'invoices-config'
            );
        }
    }

    /**
     * Register repositories in the container.
     *
     * @return void
     */
    protected function registerRepositories()
    {
        collect($this->repositoryBindings)->each(
            function ($concrete, $contract) {
                $this->app->singleton($contract, $concrete);
            }
        );
    }

    /**
     * Define the asset publishing configuration.
     *
     * @return void
     */
    protected function defineAssetPublishing()
    {
        $this->publishes(
            [
                __DIR__.'/../public' => public_path('vendor/invoices'),
            ],
            'invoices-assets'
        );
    }
}
