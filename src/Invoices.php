<?php

namespace SajiloOnline\Invoices;

/**
 * Class Invoices
 * @package SajiloOnline\Invoices
 */
class Invoices
{
    /**
     * The callback that should be used to authenticate users.
     *
     * @var \Closure
     */
    protected static $authUsing;

    /**
     * Determine if the given request can access
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return bool
     */
    public static function check($request): bool
    {
        return (static::$authUsing ?: function () {
            return app()->environment('local');
        })(
            $request
        );
    }
}
