<?php

namespace SajiloOnline\Invoices\Model;

use App\Constants\DBTable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CancelledInvoice
 * @package SajiloOnline\Invoices\Model
 *
 * @property int     $id
 * @property int     $invoice_id
 * @property int     $credit_note_number
 * @property Carbon  $credit_note_date_ad
 * @property String  $credit_note_date_bs
 * @property String  $reason_for_return
 * @property boolean $sync_with_ird
 * @property boolean $is_real_time
 * @property Carbon  $created_at
 * @property Carbon  $updated_at
 */
class CancelledInvoice extends Model
{
    /**
     * @var string
     */
    protected $table = DBTable::CANCELLED_INVOICES;

    /**
     * @var array
     */
    protected $fillable = [
        'invoice_id',
        'credit_note_number',
        'credit_note_date_ad',
        'credit_note_date_bs',
        'reason_for_return',
        'sync_with_ird',
        'is_real_time',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'sync_with_ird' => 'boolean',
        'is_real_time'  => 'boolean',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'credit_note_date_ad',
    ];
}
