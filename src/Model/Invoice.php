<?php

namespace SajiloOnline\Invoices\Model;

use App\Constants\DBTable;
use App\Data\Entities\Models\FiscalYear\FiscalYear;
use App\Data\Entities\Models\User\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class Invoice
 * @package SajiloOnline\Invoices\Model
 *
 * @property int              $id
 * @property string           $invoice_type
 * @property int              $fiscal_year_id
 * @property int              $bill_no
 * @property string           $customer_name
 * @property string           $customer_pan
 * @property string           $customer_address
 * @property Carbon           $bill_date_ad
 * @property string           $bill_date_bs
 * @property Carbon           $issue_date_ad
 * @property string           $issue_date_bs
 * @property string           $payment_method
 * @property float            $amount
 * @property float            $discount_percentage
 * @property float            $discount_amount
 * @property float            $taxable_amount
 * @property float            $tax_amount
 * @property float            $total_amount
 * @property float            $total_in_words
 * @property boolean          $sync_with_ird
 * @property boolean          $is_bill_printed
 * @property Carbon|null      $printed_time
 * @property boolean          $is_bill_active
 * @property int              $entered_by_id
 * @property int              $printed_by_id
 * @property boolean          $is_real_time
 * @property Carbon           $created_at
 * @property Carbon           $updated_at
 *
 * @property User             $entered_by
 * @property User             $printed_by
 * @property FiscalYear       $fiscal_year
 * @property Collection       $details
 * @property CancelledInvoice $cancelled_invoice
 *
 * @property string           $invoice_type_formatted
 * @property string           $payment_method_formatted
 */
class Invoice extends Model
{
    /**
     * @var string
     */
    protected $table = DBTable::INVOICES;

    /**
     * @var array
     */
    protected $fillable = [
        'invoice_type',
        'fiscal_year_id',
        'bill_no',
        'customer_name',
        'customer_pan',
        'customer_address',
        'bill_date_ad',
        'bill_date_bs',
        'issue_date_ad',
        'issue_date_bs',
        'payment_method',
        'amount',
        'discount_percentage',
        'discount_amount',
        'taxable_amount',
        'tax_amount',
        'total_amount',
        'total_in_words',
        'sync_with_ird',
        'is_bill_printed',
        'printed_time',
        'is_bill_active',
        'entered_by_id',
        'printed_by_id',
        'is_real_time',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'sync_with_ird'   => 'boolean',
        'is_bill_printed' => 'boolean',
        'is_bill_active'  => 'boolean',
        'is_real_time'    => 'boolean',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'bill_date_ad',
        'issue_date_ad',
        'printed_time',
    ];

    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        static::creating(
            function ($model) {
                $model->setAttribute('entered_by_id', isset(currentUser()->id) ? currentUser()->id : 1);
            }
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entered_by()
    {
        return $this->belongsTo(User::class, 'entered_by_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function printed_by()
    {
        return $this->belongsTo(User::class, 'printed_by_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fiscal_year()
    {
        return $this->belongsTo(FiscalYear::class, 'fiscal_year_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function details()
    {
        return $this->hasMany(Detail::class, 'invoice_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cancelled_invoice()
    {
        return $this->hasOne(CancelledInvoice::class, 'invoice_id', 'id');
    }

    /**
     * @return string
     */
    public function getInvoiceTypeFormattedAttribute(): string
    {
        $typeMapping = [
            "tax-invoice" => $this->is_bill_printed ? 'Invoice' : "Tax Invoice",
            "tax-abbr"    => $this->is_bill_printed ? 'Invoice' : "Tax Invoice",
            "income-tax"  => $this->is_bill_printed ? 'Invoice' : "Tax Invoice",
        ];

        return array_get($typeMapping, $this->invoice_type, '');
    }

    /**
     * @return string
     */
    public function getPaymentMethodFormattedAttribute(): string
    {
        $methodMapping = [
            'cash'     => 'Cash',
            'cheque'   => 'Cheque',
            'creditor' => 'Creditor',
            'other'    => 'Other',
        ];

        return array_get($methodMapping, $this->payment_method, '');
    }
}
