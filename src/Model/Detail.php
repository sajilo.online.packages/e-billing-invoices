<?php

namespace SajiloOnline\Invoices\Model;

use App\Constants\DBTable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Detail
 * @package SajiloOnline\Invoices\Model
 *
 * @property int     $id
 * @property int     $invoice_id
 * @property int     $sn
 * @property string  $particular
 * @property float   $quantity
 * @property float   $rate
 * @property float   $total
 * @property Carbon  $created_at
 * @property Carbon  $updated_at
 *
 * @property Invoice $invoice
 */
class Detail extends Model
{
    /**
     * @var string
     */
    protected $table = DBTable::INVOICE_DETAILS;

    /**
     * @var array
     */
    protected $fillable = [
        'invoice_id',
        'sn',
        'particular',
        'quantity',
        'rate',
        'total',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function invoice()
    {
        return $this->belongsTo(Invoice::class, 'invoice_id', 'id');
    }
}
