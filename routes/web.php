<?php

use Illuminate\Support\Facades\Route;


Route::get(
    '/test',
    function () {
//        event(new InvoiceAdded(app(\SajiloOnline\Invoices\Model\Invoice::class)->find(1), app(\App\Data\Repositories\Company\ProfileRepository::class)->profileData()));
//        event(new \App\Infrastructure\Events\SalesReturned(app(\SajiloOnline\Invoices\Model\Invoice::class)->find(2), app(\App\Data\Repositories\Company\ProfileRepository::class)->profileData()));
    }
);

Route::get('/', 'InvoicesController@index')->name('index');
Route::get('/create/{type}', 'InvoicesController@create')->name('create');
Route::post('/store', 'InvoicesController@store')->name('store');
Route::get('/sales-book', 'SalesBookController@index')->name('sales-book');
Route::get('/sales-return', 'SalesBookController@salesReturn')->name('sales-return');
Route::post('/sales-book/generate', 'SalesBookController@generate')->name('sales-book.generate');
Route::get('/sales-book/export/{type}', 'SalesBookController@export')->name('sales-book.export');
Route::get('/credit-note-number', 'InvoicesController@creditNoteNumber')->name('credit.note.number');
Route::get('/{invoiceId}', 'InvoicesController@show')->name('show');
Route::post('/{invoiceId}/cancel', 'InvoicesController@cancel')->name('cancel');
Route::post('/{invoiceId}/print', 'InvoicesController@print')->name('print');
