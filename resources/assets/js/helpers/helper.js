import moment from "moment";

export const numberToWord = amount => {
    const a = ["", "One ", "Two ", "Three ", "Four ", "Five ", "Six ", "Seven ", "Eight ", "Nine ", "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen "];
    const b = ["", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"];

    amount = (amount.toFixed(2)).toString().split(".");
    let number = amount[0].split(",").join("");
    let precision = amount.length > 1 ? amount[1].split(",").join("") : "00";

    if ((number = number.toString()).length > 9) return "overflow";
    let n = ("000000000" + number).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    let p = ("00" + precision).substr(-2).match(/^(\d{2})$/);

    if (!n) {
        return;
    }

    let word = "";
    word += (Number(n[1]) !== 0) ? (a[Number(n[1])] || b[n[1][0]] + " " + a[n[1][1]]) + "Crore " : "";
    word += (Number(n[2]) !== 0) ? (a[Number(n[2])] || b[n[2][0]] + " " + a[n[2][1]]) + "Lakh " : "";
    word += (Number(n[3]) !== 0) ? (a[Number(n[3])] || b[n[3][0]] + " " + a[n[3][1]]) + "Thousand " : "";
    word += (Number(n[4]) !== 0) ? (a[Number(n[4])] || b[n[4][0]] + " " + a[n[4][1]]) + "Hundred " : "";
    word += (Number(n[5]) !== 0) ? ((word !== "") ? "and " : "") + (a[Number(n[5])] || b[n[5][0]] + " " + a[n[5][1]]) + "Ruppees, " : "";

    word += (Number(p[1]) !== 0) ? (a[Number(p[1])] || b[p[1][0]] + " " + a[p[1][1]]) + "Paisa only " : "";

    return word;
};

export const printDivById = divId => {
    const myWindow = window.open("", "PRINT", "height=800,width=1000");

    myWindow.document.write("<html><head><title></title>");

    document.querySelectorAll("link[media=print]").forEach(link => {
        myWindow.document.write("<link rel='stylesheet' type='text/css' href='" + link.href + "'>");
    });

    myWindow.document.write("</head><body >");

    let printTime = moment().format("YYYY-MM-DD HH:mm:ss"),
        printableContent = document.getElementById(divId).innerHTML;

    printableContent = printableContent.replace(/__PRINT_TIME__/g, printTime);
    myWindow.document.write(printableContent);

    myWindow.document.write("</body></html>");

    myWindow.document.close(); // necessary for IE >= 10
    myWindow.focus(); // necessary for IE >= 10*/

    myWindow.print();

    if (myWindow.matchMedia) {
        let mediaQueryList = myWindow.matchMedia("print");
        mediaQueryList.addListener(mql => {
            if (mql.matches) {
                triggerNativeEvent(document, "beforePrint");
            } else {
                myWindow.close();
                triggerNativeEvent(document, "afterPrint");
            }
        });
    }

    return true;
};

export const triggerNativeEvent = (el, eventName) => {
    if (el.fireEvent) { // < IE9
        (el.fireEvent("on" + eventName));
    } else {
        let event = document.createEvent("Events");
        event.initEvent(eventName, true, false);
        el.dispatchEvent(event);
    }
};

export const toQueryString = object => {
    if (typeof(object) !== "object") {
        return "";
    }
    return `?${Object.keys(object).map(k => `${k}=${object[k]}`).join("&")}`;
};
