import Vue         from "vue";
import InvoiceForm from "./pages/Invoices/InvoiceForm";
import Invoice     from "./pages/Invoices/Invoice";
import SalesBook   from "./pages/SalesBook/SalesBook";

try {
    require("jquery-ui");
    require("./lib/nepali-date-picker/jquery.nepaliDatePicker.min");
} catch (e) {}

window.Bus = new Vue({name: "Bus"});

Vue.component("invoice-form", InvoiceForm);
Vue.component("invoice", Invoice);
Vue.component("sales-book", SalesBook);

Vue.config.errorHandler = (err, vm, info) => {
    console.error(err);
};

Vue.mixin({
    methods: {
        numberOnly(event) {
            if ((event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) && event.charCode !== 46) {
                event.preventDefault();
            }

            if (event.target.value.indexOf(".") !== -1 && event.charCode === 46) {
                event.preventDefault();
            }

            return true;
        },
    },
});

new Vue({
    el: "#invoice-app",
});
