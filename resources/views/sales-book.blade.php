@extends('web.layouts.no-vue-app')

@section('title', $type === 'sales-book' ? 'Sales Book' : 'Sales Return')
@section('heading', $type === 'sales-book' ? 'Sales Book' : 'Sales Return')

@section('content')
	<div id="invoice-app">
		<sales-book generate-api="{{ route('invoices.sales-book.generate') }}" export-api="{{ route('invoices.sales-book.export', '__TYPE__') }}" type="{{ $type }}"/>
	</div>
@endsection

@push('css')
	<link rel="stylesheet" type="text/css" href="{{ mix('css/app.css', 'vendor/invoices') }}">
@endpush

@push('scripts')
	<script src="{{ mix('js/manifest.js', 'vendor/invoices') }}"></script>
	<script src="{{ mix('js/vendor.js', 'vendor/invoices') }}"></script>
	<script src="{{ mix('js/app.js', 'vendor/invoices') }}"></script>
@endpush
