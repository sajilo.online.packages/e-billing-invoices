@extends('web.layouts.no-vue-app')

@php /** @var \SajiloOnline\Invoices\Model\Invoice $invoice */ @endphp

@section('title', 'Invoice #'.$invoice->bill_no)
@section('heading', 'Invoice #'.$invoice->bill_no )

@section('content')
	<div id="invoice-app">
		<div class="row printable">
			<div class="col-md-12">
				
				<invoice inline-template print-status-api="{{ route('invoices.print', $invoice->id) }}" :is-bill-printed="{{ $invoice->is_bill_printed ? 'true' : 'false' }}">
					<div @keyup.native.alt.67="printShortCutHandler" class="panel panel-default invoice">
						<div class="panel-heading clearfix">
							<div class="print">
								@if($invoice->is_bill_active)
									<button class="tip btn btn-danger" title="" data-original-title="Cancel invoice" @click="showCancelFormModal"><i class="s24 fa fa-times"></i></button>
								@else
									<label class="tip btn btn-danger s18">Cancelled</label>
								@endif
							</div>
							
							<div class="print">
								<button class="tip btn btn-default" @click="printInvoice" title="" data-original-title="Print invoice"><i class="s24 fa fa-print"></i></button>
							</div>
						</div>
						
						<div class="panel-body" id="print-invoice">
							<div class="invoice-form">
								
								<div class="header">
									<div class="row mb20">
										<div class="col-sm-12 title">
											{{ $invoice->invoice_type_formatted }} @{{ copy_of_original ? '(Copy of Original)' : '' }}
											@if(!$invoice->is_bill_active)
												- <span class="color-red">Cancelled</span>
											@endif
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6 col-print-6 content-left">
											<div class="item">
												<label for="bill_num">Bill No.:</label>
												<span class="text color-red-dark" id="bill_num">{{ $invoice->bill_no }}</span>
											</div>
											<div class="item">
												<label for="seller_pan">Seller’s PAN :</label>
												<span class="text" id="seller_pan">{{ $seller->get('pan') }}</span>
											</div>
											<div class="item">
												<label for="seller_name">Seller’s Name:</label>
												<span class="text" id="seller_name">{{ $seller->get('name') }}</span>
											</div>
											<div class="item">
												<label for="address">Address:</label>
												<span class="text" id="seller_address"></span>
											</div>
											<div class="item">
												<label for="purchaser_name">Purchaser’s Name:</label>
												<span class="text" id="purchaser_name">{{ $invoice->customer_name }}</span>
											</div>
											<div class="item">
												<label for="purchaser_address">Address:</label>
												<span class="text" id="purchaser_address">{{ $invoice->customer_address }}</span>
											</div>
											<div class="item">
												<label for="purchaser_pan">Purchaser’s PAN:</label>
												<span class="text" id="purchaser_pan">{{ $invoice->customer_pan }}</span>
											</div>
										</div>
										<div class="col-sm-6 col-sm-6 content-right">
											<div class="item">
												<label for="trans_date_ad">Transactions Date :</label>
												<span class="help-text">AD: </span>
												<span class="text date" id="trans_date_ad">{{ $invoice->bill_date_ad->toDateString() }}</span>
												<span class="help-text">(BS: </span>
												<span class="text date" id="trans_date_bs">{{ $invoice->bill_date_bs }}</span>
												<span class="help-text">)</span>
											</div>
											<div class="item">
												<label for="issued_date_ad">Invoice Issue Date:</label>
												<span class="help-text">AD: </span>
												<span class="text date" id="issued_date_ad">{{ $invoice->issue_date_ad->toDateString() }}</span>
												<span class="help-text">(BS: </span>
												<span class="text date" id="issued_date_bs">{{ $invoice->issue_date_bs }}</span>
												<span class="help-text">)</span>
											</div>
											<div class="item">
												<label for="payment_method">Method of payment:</label>
												<span class="text" id="payment_method">{{ $invoice->payment_method_formatted }}</span>
											</div>
											
											<div class="print-only">
												<hr class="divider"/>
												
												<div class="item">
													<label for="printed_by">Printed By:</label>
													<span class="text" id="printed_by">{{ currentUser()->username }}</span>
												</div>
												<div class="item">
													<label for="print_time">Print Time:</label>
													<span class="text" id="print_time">__PRINT_TIME__</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="body">
									<div class="table-responsive">
										<table class="table table-bordered table-condensed">
											<thead>
											<tr>
												<th rowspan="2" width="5%">S.No.</th>
												<th rowspan="2" width="40%">Details</th>
												<th rowspan="2" width="15%">Quantity</th>
												<th width="15%">Per Unit</th>
												<th width="20%">Total</th>
											</tr>
											<tr>
												<th>Amount (Rs.)</th>
												<th>Amount (Rs.)</th>
											</tr>
											</thead>
											
											<tbody>
											@foreach($invoice->details as $detail)
												@php /** @var \SajiloOnline\Invoices\Model\Detail $detail*/ @endphp
												<tr>
													<td class="text-center">
														<span>{{ $detail->sn }}</span>
													</td>
													<td>
														<span>{{ $detail->particular }}</span>
													</td>
													<td class="text-right">
														<span>{{ $detail->quantity }}</span>
													</td>
													<td class="text-right">
														<span>{{ amountFormat($detail->rate) }}</span>
													</td>
													<td class="text-right">
														<span>{{ amountFormat($detail->total) }}</span>
													</td>
												</tr>
											@endforeach
											</tbody>
											
											<tfoot>
											<tr>
												<td colspan="4" class="text-right pr10">
													<span>Discount</span>
													<span>{{ round($invoice->discount_percentage, 2) }}</span>
													<span>%</span>
												</td>
												<td class="text-right">
													<span>{{ amountFormat($invoice->discount_amount) }}</span>
												</td>
											</tr>
											
											@if($invoice->invoice_type === \SajiloOnline\Invoices\Constants\InvoiceType::INCOME_TAX)
												<tr>
													<td colspan="4" class="text-right pr10">Taxable Amount</td>
													<td class="text-right">
														<span>{{ amountFormat($invoice->taxable_amount) }}</span>
													</td>
												</tr>
											@endif
											
											@if($invoice->invoice_type === \SajiloOnline\Invoices\Constants\InvoiceType::TAX_INVOICE)
												<tr>
													<td colspan="4" class="text-right pr10">VAT 13 %</td>
													<td class="text-right">
														<span>{{ amountFormat($invoice->tax_amount) }}</span>
													</td>
												</tr>
											@endif
											
											<tr>
												<td colspan="4" class="text-right pr10"><strong>Total</strong></td>
												<td class="text-right">
													<strong>{{ amountFormat($invoice->total_amount) }}</strong>
												</td>
											</tr>
											</tfoot>
										</table>
									</div>
								</div>
								
								<div class="footer">
									<div class="row">
										<div class="col-sm-8 col-print-8">
											<span>( In Words: </span>
											<strong>{{ $invoice->total_in_words }}</strong>
											<span> )</span>
										</div>
										<div class="col-sm-4 col-print-4">
											<strong class="authorized">Authorized Signature</strong>
										</div>
									</div>
								</div>
							
							</div>
						</div>
						
						<invoice-cancel-form v-if="showCancelForm"
											 @close="hideCancelFormModal"
											 api-credit-note-number="{{ route('invoices.credit.note.number') }}"
											 api-cancellation="{{ route('invoices.cancel', $invoice->id) }}"
											 :invoice="{{ $invoice->toJson() }}"></invoice-cancel-form>
					</div>
				</invoice>
			
			</div>
		</div>
	</div>
@endsection

@push('css')
	<link rel="stylesheet" type="text/css" href="{{ mix('css/app.css', 'vendor/invoices') }}">
	<link rel="stylesheet" media="print" type="text/css" href="{{ mix('css/print.css', 'vendor/invoices') }}">
@endpush

@push('scripts')
	<script src="{{ mix('js/manifest.js', 'vendor/invoices') }}"></script>
	<script src="{{ mix('js/vendor.js', 'vendor/invoices') }}"></script>
	<script src="{{ mix('js/app.js', 'vendor/invoices') }}"></script>
@endpush
