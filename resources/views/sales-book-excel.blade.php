<html>
<head>
	<title>{{ $reportType === 'active' ? 'Sales Book' : 'Sales Return' }}</title>
	<style>
		table thead th {
			background: #000;
		}
	</style>
</head>
<body>
<div class="sales-book">
	<table>
		<tr>
			<td>Name of Firm:</td>
			<td colspan="4">{{ $seller->get('name') }}</td>
		</tr>
		<tr>
			<td>PAN :</td>
			<td colspan="4">{{ $seller->get('pan') }}</td>
		</tr>
		<tr>
			<td>Duration of Sales:</td>
			<td colspan="4">{{ month_name($dateType, $month) }}, {{ $year }}</td>
		</tr>
	</table>
	
	<table>
		<thead>
		<tr bgcolor="#5f9ea0">
			<th colspan="4" bgcolor="#5f9ea0">Invoice</th>
			<th rowspan="2" bgcolor="#5f9ea0">Total Sales</th>
			<th rowspan="2" bgcolor="#5f9ea0">Non Taxable Sales</th>
			<th rowspan="2" bgcolor="#5f9ea0">Export Sales</th>
			<th rowspan="2" bgcolor="#5f9ea0">Discount</th>
			<th colspan="2" bgcolor="#5f9ea0">Taxable Sales</th>
		</tr>
		<tr>
			<th>Date</th>
			<th>Bill No.</th>
			<th>Buyer's Name</th>
			<th>Buyer's PAN Number</th>
			<th>Amount</th>
			<th>Tax (Rs)</th>
		</tr>
		</thead>
		
		<tbody>
		@forelse($invoices as $invoice)
			@php /** @var \SajiloOnline\Invoices\Model\Invoice $invoice*/ @endphp
			<tr>
				<td class="text-center">
					{{ $dateType === 'bs' ? $invoice->bill_date_bs : $invoice->bill_date_ad->toDateString() }}
				</td>
				<td>{{ $invoice->bill_no }}</td>
				<td>{{ $invoice->customer_name }}</td>
				<td>{{ $invoice->customer_pan }}</td>
				<td class="text-right">{{ amountFormat($invoice->total_amount) }}</td>
				<td class="text-right"></td>
				<td class="text-right"></td>
				<td class="text-right">{{ amountFormat($invoice->discount_amount) }}</td>
				<td class="text-right">{{ amountFormat($invoice->taxable_amount) }}</td>
				<td class="text-right">{{ amountFormat($invoice->tax_amount) }}</td>
			</tr>
		@empty
			<tr>
				<td colspan="10">No transaction for selected duration.</td>
			</tr>
		@endforelse
		</tbody>
		
		@if($invoices->count())
			<tfoot class="strong">
			<tr>
				<td colspan="4">Total</td>
				<td class="text-right">{{ calculate_total($invoices, 'total_amount') }}</td>
				<td></td>
				<td></td>
				<td class="text-right">{{ calculate_total($invoices, 'discount_amount') }}</td>
				<td class="text-right">{{ calculate_total($invoices, 'taxable_amount') }}</td>
				<td class="text-right">{{ calculate_total($invoices, 'tax_amount') }}</td>
			</tr>
			</tfoot>
		@endif
	</table>
</div>
</body>
</html>
