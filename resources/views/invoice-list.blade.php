@extends('web.layouts.app')

@section('title', 'Invoice List')
@section('heading', 'Invoice List')

@section('content')
	<div id="invoice-app">
		<div class="row">
			<div class="col-md-12">
				<div class="invoice-form">
					
					<div class="tabs mb20">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#active" data-toggle="tab" aria-expanded="true">Active</a>
							</li>
							<li class="">
								<a href="#sales-return" data-toggle="tab" aria-expanded="false">Sales Return</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane fade active in" id="active">
								<div class="table-responsive">
									@include('invoices::partials.active-invoices-table', compact('activeInvoices'))
								</div>
							</div>
							<div class="tab-pane fade" id="sales-return">
								<div class="table-responsive">
									@include('invoices::partials.cancelled-invoices-table')
								</div>
							</div>
						</div>
					</div>
				
				</div>
			</div>
		</div>
	</div>
@endsection

@push('css')
	<link rel="stylesheet" type="text/css" href="{{ mix('css/app.css', 'vendor/invoices') }}">
@endpush
