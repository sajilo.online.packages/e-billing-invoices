@extends('web.layouts.no-vue-app')

@section('title', 'Add New Invoice')
@section('heading', 'Add New Invoice')

@section('content')
	<div id="invoice-app">
		<div class="row">
			<div class="col-md-12">
				<invoice-form type="{{ $invoiceType }}"
							  :seller="{{ $seller }}"
							  invoice-save-api="{{ route('invoices.store') }}"></invoice-form>
			</div>
		</div>
	</div>
@endsection

@push('css')
	<link rel="stylesheet" type="text/css" href="{{ mix('css/app.css', 'vendor/invoices') }}">
@endpush

@push('scripts')
	<script src="{{ mix('js/manifest.js', 'vendor/invoices') }}"></script>
	<script src="{{ mix('js/vendor.js', 'vendor/invoices') }}"></script>
	<script src="{{ mix('js/app.js', 'vendor/invoices') }}"></script>
@endpush
