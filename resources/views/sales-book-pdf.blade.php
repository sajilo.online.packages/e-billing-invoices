<html>
<head>
	<title>{{ $reportType === 'active' ? 'Sales Book' : 'Sales Return' }}</title>
	<style>
		body {
			font-size: 14px;
			line-height: 1.5;
			color: #333;
			background-color: #fff;
			border: 1px solid #000;
		}
		
		.strong {
			font-weight: bold;
		}
		
		.sales-book {
			padding: 15px;
		}
		
		.sales-book .header .title {
			text-align: center;
			font-weight: bold;
			font-size: 1.2em;
		}
		
		.sales-book .header .item {
			display: -webkit-box;
			display: -ms-flexbox;
			display: block;
			width: 300px;
			margin-bottom: 5px;
		}
		
		.sales-book .header .item label {
			white-space: nowrap;
			font-weight: 700;
		}
		
		.sales-book .header .item span.text {
			margin-left: 5px;
			padding-left: 5px;
		}
		
		.sales-book .header .item span.text.color-red-dark {
			color: #ec693c;
		}
		
		.sales-book .header .item span.text.date {
			margin: 0;
			padding: 0;
			white-space: nowrap;
		}
		
		.sales-book .body .table-responsive {
			width: 100%;
		}
		
		.sales-book .body .table-responsive .table {
			border: 1px solid #c4c4c4;
			width: 100%;
			max-width: 100%;
			margin-bottom: 18px;
			border-collapse: collapse;
			font-size: 14px;
		}
		
		.sales-book .body .table-responsive .table thead tr {
			background: #f3f3f3;
		}
		
		.sales-book .body .table-responsive .table thead tr th {
			text-align: center;
		}
		
		.sales-book .body .table-responsive .table tbody tr td,
		.sales-book .body .table-responsive .table tbody tr th,
		.sales-book .body .table-responsive .table thead tr td,
		.sales-book .body .table-responsive .table thead tr th,
		.sales-book .body .table-responsive .table tfoot tr td,
		.sales-book .body .table-responsive .table tfoot tr th {
			padding: 5px 10px;
			vertical-align: middle;
			border: 1px solid #c4c4c4;
		}
		
		.sales-book .footer .authorized {
			display: block;
			text-align: center;
			border-top: 1px dashed #333;
			margin: 20px;
			padding-top: 10px;
		}
	</style>
</head>
<body>
<div class="sales-book">
	@include('invoices::partials.sales-book-table', compact(
			'invoices',
			'seller',
			'year',
			'month',
			'dateType',
			'reportType'
		))
</div>
</body>
</html>
