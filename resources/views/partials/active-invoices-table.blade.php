<table class="table table-bordered table-condensed table-hover">
	<thead>
	<tr>
		<th>Bill No.</th>
		<th>Transaction Date</th>
		<th>Issued Date</th>
		<th>Customer</th>
		<th>Total Amount (Rs.)</th>
		<th></th>
	</tr>
	</thead>
	
	<tbody>
	@forelse($activeInvoices as $invoice)
		@php /** @var \SajiloOnline\Invoices\Model\Invoice $invoice*/ @endphp
		<tr>
			<td>{{ $invoice->bill_no }}</td>
			<td>
				<p>AD: {{ $invoice->bill_date_ad->toDateString() }}</p>
				<p>BS: {{ $invoice->bill_date_bs }}</p>
			</td>
			<td>
				<p>AD: {{ $invoice->issue_date_ad->toDateString() }}</p>
				<p>BS: {{ $invoice->issue_date_bs }}</p>
			</td>
			<td>
				<p>{{ $invoice->customer_name }} (PAN: {{ $invoice->customer_pan }})</p>
				<p>{{ $invoice->customer_address }}</p>
			</td>
			<td class="text-right">{{ amountFormat($invoice->total_amount) }}</td>
			<td>
				<a href="{{ route('invoices.show', $invoice->id) }}" class="btn btn-xs btn-info">
					<i class="fa fa-eye"></i>
				</a>
			</td>
		</tr>
	@empty
		<tr>
			<td colspan="6">No invoices in the record.</td>
		</tr>
	@endforelse
	</tbody>
</table>
