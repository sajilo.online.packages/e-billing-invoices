<div class="header">
	<div class="row mb20">
		<div class="col-sm-12 title">{{ $reportType === 'active' ? 'Sales Book' : 'Sales Return' }}</div>
	</div>
	<div class="row">
		<div class="col-sm-6 content-left">
			<div class="item">
				<label>Name of Firm:</label>
				<span class="text ">{{ $seller->get('name') }}</span>
			</div>
			<div class="item">
				<label>PAN :</label>
				<span class="text">{{ $seller->get('pan') }}</span>
			</div>
			<div class="item">
				<label>Duration of Sales:</label>
				<span class="text">{{ month_name($dateType, $month) }}, {{ $year }}</span>
			</div>
		</div>
		<div class="col-sm-6 content-right">
		</div>
	</div>
</div>

<div class="body">
	<div class="table-responsive">
		<table class="table table-bordered table-condensed">
			<thead>
			<tr>
				<th colspan="4">Invoice</th>
				<th rowspan="2">Total Sales</th>
				<th rowspan="2">Non Taxable Sales</th>
				<th rowspan="2">Export Sales</th>
				<th rowspan="2">Discount</th>
				<th colspan="2">Taxable Sales</th>
			</tr>
			<tr>
				<th>Date</th>
				<th>Bill No.</th>
				<th>Buyer's Name</th>
				<th>Buyer's PAN Number</th>
				<th>Amount</th>
				<th>Tax (Rs)</th>
			</tr>
			</thead>
			
			<tbody>
			@forelse($invoices as $invoice)
				@php /** @var \SajiloOnline\Invoices\Model\Invoice $invoice*/ @endphp
				<tr>
					<td class="text-center">
						{{ $dateType === 'bs' ? $invoice->bill_date_bs : $invoice->bill_date_ad->toDateString() }}
					</td>
					<td>{{ $invoice->bill_no }}</td>
					<td>{{ $invoice->customer_name }}</td>
					<td>{{ $invoice->customer_pan }}</td>
					<td class="text-right">{{ amountFormat($invoice->total_amount) }}</td>
					<td class="text-right"></td>
					<td class="text-right"></td>
					<td class="text-right">{{ amountFormat($invoice->discount_amount) }}</td>
					<td class="text-right">{{ amountFormat($invoice->taxable_amount) }}</td>
					<td class="text-right">{{ amountFormat($invoice->tax_amount) }}</td>
				</tr>
			@empty
				<tr>
					<td colspan="10">No transaction for selected duration.</td>
				</tr>
			@endforelse
			</tbody>
			
			@if($invoices->count())
				<tfoot class="strong">
				<tr>
					<td colspan="4">Total</td>
					<td class="text-right">{{ calculate_total($invoices, 'total_amount') }}</td>
					<td></td>
					<td></td>
					<td class="text-right">{{ calculate_total($invoices, 'discount_amount') }}</td>
					<td class="text-right">{{ calculate_total($invoices, 'taxable_amount') }}</td>
					<td class="text-right">{{ calculate_total($invoices, 'tax_amount') }}</td>
				</tr>
				</tfoot>
			@endif
		</table>
	</div>
</div>
