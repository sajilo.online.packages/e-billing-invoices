<table class="table table-bordered table-condensed table-hover">
	<thead>
	<tr>
		<th>Ref bill no.</th>
		<th>Credit note no.</th>
		<th>Credit note date</th>
		<th>Reason for return</th>
		<th></th>
	</tr>
	</thead>
	
	<tbody>
	@forelse($cancelledInvoices as $invoice)
		@php /** @var \SajiloOnline\Invoices\Model\Invoice $invoice*/ @endphp
		<tr>
			<td>{{ $invoice->bill_no }}</td>
			<td>{{ $invoice->cancelled_invoice->credit_note_number }}</td>
			<td>
				<p>AD: {{ $invoice->cancelled_invoice->credit_note_date_ad->toDateString() }}</p>
				<p>BS: {{ $invoice->cancelled_invoice->credit_note_date_bs }}</p>
			</td>
			<td>{{ $invoice->cancelled_invoice->reason_for_return }}</td>
			<td>
				<a href="{{ route('invoices.show', $invoice->id) }}" class="btn btn-xs btn-info">
					<i class="fa fa-eye"></i>
				</a>
			</td>
		</tr>
	@empty
		<tr>
			<td colspan="5">No sales return in the record.</td>
		</tr>
	@endforelse
	</tbody>
</table>
